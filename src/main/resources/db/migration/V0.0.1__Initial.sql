CREATE TABLE products
(
    sku         VARCHAR(16)     NOT NULL
        CONSTRAINT pk_product_id PRIMARY KEY,
    name        VARCHAR(125)    NOT NULL,
    description VARCHAR(125),
    price       DECIMAL           NOT NULL,
    created_at  TIMESTAMP     NOT NULL,
    updated_at  TIMESTAMP     NOT NULL
);

INSERT INTO products (sku, name, description, price, created_at, updated_at)
VALUES ('tv', 'Television', 'Sumsung TV', 549.99, '2022-11-08 11:22:20.704086', '2022-11-08 11:22:20.704086');

INSERT INTO products (sku, name, description, price, created_at, updated_at)
VALUES ('iphone5', 'IPhone 5', 'Apple Iphone 5', 99.99, '2022-11-08 11:22:20.704086', '2022-11-08 11:22:20.704086');
