package de.imedia24.shop.service

import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductPatchRequest
import de.imedia24.shop.domain.product.ProductPostRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import org.springframework.stereotype.Service

@Service
class ProductService(private val productRepository: ProductRepository) {

    fun findProductBySku(sku: String): ProductResponse? {
        return productRepository.findBySku(sku)?.toProductResponse()
    }

    fun findProductsBySkus(skus: List<String>) : List<ProductResponse?> {
        return skus.map { sku -> findProductBySku(sku) }
    }

    fun findAllProducts() : List<ProductResponse> {
        return  productRepository.findAll().map { productEntity -> productEntity.toProductResponse() }
    }

    fun addProduct(product: ProductPostRequest): ProductResponse {
        return productRepository.save(product.toProductEntity()).toProductResponse()
    }

    fun updateProduct(sku: String, newProduct: ProductPatchRequest): ProductResponse? {
        val currentProduct = productRepository.findBySku(sku)
        if (currentProduct != null) {
            return productRepository.save(newProduct.toProductEntity(currentProduct)).toProductResponse()
        }
        return null
    }
 }
