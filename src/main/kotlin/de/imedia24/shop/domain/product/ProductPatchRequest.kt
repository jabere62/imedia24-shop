package de.imedia24.shop.domain.product

import de.imedia24.shop.db.entity.ProductEntity
import java.math.BigDecimal
import java.time.ZonedDateTime

class ProductPatchRequest(
        val name: String?,
        val description: String?,
        val price: BigDecimal?,
) {
    fun toProductEntity(current: ProductEntity): ProductEntity {
        return ProductEntity(
                sku = current.sku,
                name = name ?: current.name,
                description = description ?: current.description,
                price = price ?: current.price,
                stock = current.stock,
                createdAt = current.createdAt,
                updatedAt = ZonedDateTime.now()
        )
    }
}