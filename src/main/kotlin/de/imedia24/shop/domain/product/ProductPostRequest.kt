package de.imedia24.shop.domain.product

import de.imedia24.shop.db.entity.ProductEntity
import java.math.BigDecimal
import java.time.ZonedDateTime

data class ProductPostRequest(
        val sku: String,
        val name: String,
        val description: String?,
        val price: BigDecimal,
        val stock: Int
) {
    fun toProductEntity(): ProductEntity {
        return ProductEntity(
                sku,
                name,
                description ?: "",
                price,
                stock,
                ZonedDateTime.now(),
                ZonedDateTime.now()
        )
    }
}