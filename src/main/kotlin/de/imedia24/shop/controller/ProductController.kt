package de.imedia24.shop.controller

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductPatchRequest
import de.imedia24.shop.domain.product.ProductPostRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import javax.websocket.server.PathParam

@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")
        val product = productService.findProductBySku(sku)
        return if(product == null) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found!") //ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @GetMapping("/products", produces = ["application/json;charset=utf-8"])
    fun findProductsBySkus(@RequestParam("skus", required = false) skus: List<String>?): ResponseEntity<List<ProductResponse?>> {
        return if(skus == null) {
            logger.info("Request to get all products")
            ResponseEntity.ok(productService.findAllProducts())
        } else {
            logger.info("Request to get products by skus=$skus")
            ResponseEntity.ok(productService.findProductsBySkus(skus))
        }
    }

    @PostMapping("/products", produces = ["application/json;charset=utf-8"])
    fun addProduct(@RequestBody product: ProductPostRequest): ResponseEntity<ProductResponse> {
        logger.info("Request to add product $product")
        return if(productService.findProductBySku(product.sku) != null) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Sku already exists!")
        } else {
            val savedProduct = productService.addProduct(product)
            ResponseEntity.ok(savedProduct);
        }
    }

    @PatchMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun updateProduct(
            @PathVariable sku: String,
            @RequestBody product: ProductPatchRequest
    ): ResponseEntity<ProductResponse> {
        logger.info("Request to update product with sku=$sku")
        val newProduct = productService.updateProduct(sku, product)
        return if(newProduct == null) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found!")
        } else {
            ResponseEntity.ok(newProduct)
        }
    }
}
