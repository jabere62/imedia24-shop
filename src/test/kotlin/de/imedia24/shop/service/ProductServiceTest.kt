package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import java.math.BigDecimal
import java.time.ZonedDateTime

class ProductServiceTest {
    val productRepositoryMock: ProductRepository = mockk()
    val productService = ProductService(productRepositoryMock)

    @Test
    fun shouldReturnProductOfGivenSku() {
        // Given
        val sku = "tv"
        val product = ProductEntity(
                "tv",
                "Television",
                "Sumsung Television",
                BigDecimal(549.99),
                0,
                ZonedDateTime.now(),
                ZonedDateTime.now()
        )
        every { productRepositoryMock.findBySku(sku) } returns product

        // When
        val result = productService.findProductBySku(sku)

        // Should
        verify(exactly = 1) { productRepositoryMock.findBySku(sku) }
        assertEquals(result, product.toProductResponse())
    }

}