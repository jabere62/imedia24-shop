package de.imedia24.shop.controller

import com.ninjasquad.springmockk.MockkBean
import de.imedia24.shop.domain.product.ProductPatchRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import io.mockk.every
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.patch
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.math.BigDecimal

@WebMvcTest
class ProductControllerTest(@Autowired val mockMvc: MockMvc) {

    @MockkBean
    lateinit var productService: ProductService

    @Test
    fun `given a sku, should return product json with status OK`() {
        //Given
        val sku = "tv";
        val productResponse = ProductResponse("tv", "TV", "TV", BigDecimal(549.99), 100)
        every { productService.findProductBySku(sku) } returns productResponse

        // Should
        mockMvc.get("/products/" + sku)
                .andExpect { status().isOk }
                .andExpect { content().contentType(MediaType.APPLICATION_JSON) }
                .andExpect {
                    jsonPath("$.sku").value("tv")
                    jsonPath("$.stock").value("100")
                }
    }

    @Test
    fun `given a sku and new product, should return an updated product json with status OK`() {
        //Given
        val sku = "tv";
        val newProduct = ProductPatchRequest("Television", null, null)
        val productResponse = ProductResponse("tv", "Television", "TV", BigDecimal(549.99), 100)
        every { productService.updateProduct(sku, newProduct) } returns productResponse

        // Should
        mockMvc.patch("/products/" + sku, { newProduct })
                .andExpect { status().isOk }
                .andExpect { content().contentType(MediaType.APPLICATION_JSON) }
                .andExpect {
                    jsonPath("$.name").value("Television")
                    jsonPath("$.stock").value("100")
                }
    }

}